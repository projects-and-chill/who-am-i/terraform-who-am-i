terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.6.0"
    }
  }
}


resource "aws_alb" "alb" {
  name = "alb-${var.ressource_name_suffix}"
  load_balancer_type = "application"
  ip_address_type = "ipv4"
  internal = false
  security_groups = var.elb_security_groups
  subnets = var.elb_subnets
  tags = var.resources_tags
}


resource "aws_alb_target_group" "api_group" {
  name = "tg-api-${var.ressource_name_suffix}"
  port = 80
  protocol = "HTTP"
  protocol_version = "HTTP1"
  target_type = "instance"
  vpc_id = var.vpc_id
  stickiness {
    cookie_name = "app_cookie_api"
    type = "app_cookie"
    enabled = true
    cookie_duration = 3600
  }
  health_check {
    enabled = true
    interval = 30
    path = "/${var.api_name}/test"
    port = "traffic-port"
    protocol = "HTTP"
    timeout = 2
    healthy_threshold = 2
    unhealthy_threshold = 2
    matcher = "200-299"
  }
  tags = var.resources_tags
}

resource "aws_alb_target_group" "front_group" {
  name = "tg-front-${var.ressource_name_suffix}"
  port = 80
  protocol = "HTTP"
  protocol_version = "HTTP1"
  target_type = "instance"
  vpc_id = var.vpc_id
  stickiness {
    cookie_name = "app_cookie_front"
    type = "app_cookie"
    enabled = true
    cookie_duration = 3600
  }
  health_check {
    enabled = true
    interval = 30
    path = "/"
    port = "traffic-port"
    protocol = "HTTP"
    timeout = 2
    healthy_threshold = 2
    unhealthy_threshold = 2
    matcher = "200-299"
  }
  tags = var.resources_tags
}


resource "aws_lb_listener" "redirect_http_to_https" {
  load_balancer_arn = aws_alb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}


resource "aws_alb_listener" "main_listener" {
  load_balancer_arn = aws_alb.alb.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   =  var.ssl_certificate

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.front_group.arn
  }
}


resource "aws_alb_listener_rule" "api_path_rules" {
  listener_arn = aws_alb_listener.main_listener.arn
  priority = 1

  condition {
    path_pattern {
      values = [
        "/${var.api_name}/*",
        "/socket.io/*"
      ]
    }
  }

  action {
    type = "forward"
    target_group_arn = aws_alb_target_group.api_group.arn
  }

}
