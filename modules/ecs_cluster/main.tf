terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.6.0"
    }
  }
}


resource "aws_ecs_cluster" "cluster" {
  name = var.cluster_name
  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

locals {
  user_data = <<-EOT
#!/bin/bash -xe
echo ECS_CLUSTER=${var.cluster_name} >> /etc/ecs/ecs.config;
EOT
}

resource "aws_launch_template" "launch_template_ecs" {
  name =  "ecs-template-cluster-${var.cluster_name}"
  image_id = var.ami_id
  instance_type = var.instance_type
  key_name = var.key_name
  iam_instance_profile {
    arn = aws_iam_instance_profile.container_instance_profile.arn
  }
  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "ecs-abt"
      schedule = var.scheduling_rule_name
    }
  }
  user_data = base64encode(local.user_data)
}


resource "aws_instance" "ec2_ecs" {
  # Une bien meilleur alternative aurait été d'utiliser un "capacity provider" pour répartir
  # équitablement le déploimeent des instances ec2 sur les différents subnets disponibles.
  subnet_id = var.subnets_ids[0]
  vpc_security_group_ids = var.sg_ids
  launch_template {
    id = aws_launch_template.launch_template_ecs.id
    version = aws_launch_template.launch_template_ecs.latest_version
  }
  tags = merge(var.resources_tags, {
    Name = "ecs-${var.ressource_name_suffix}"
  })
}



/*--------------- Roles iams ----------------------*/
resource "aws_iam_role" "container_role" {
  name = "ec2-container-role-${var.ressource_name_suffix}"
  description = "Allows EC2 instances to call AWS services on your behalf"
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"]
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Effect : "Allow"
      Action = "sts:AssumeRole"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
  })

}

resource "aws_iam_instance_profile" "container_instance_profile" {
  name = "InstanceProfile-EC2containerServices-${var.ressource_name_suffix}"
  role = aws_iam_role.container_role.name
}
