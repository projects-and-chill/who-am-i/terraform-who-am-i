output "vpc_id" {
  value = module.vpc.vpc_id
  description = "Main vcp id"
}

output "public_subnet_ids" {
  value = module.vpc.public_subnet_ids
  description = "list of subnet ids"
}

output "elb_url" {
  value = module.elb.elb_url
  description = "elb dns name"
}
